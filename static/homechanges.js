$(document).ready(function() {

  $(".PR1").mouseenter(function() {
    if ($(".box").hasClass("PR1box")) {

      $(".box").removeClass("PR1box");
      $("body").removeClass("PR1change");

    }
    else {
      $(".box").toggleClass("PR1box");
      $("body").toggleClass("PR1change");
    }
    if ($("body").hasClass("PR3change") || $("body").hasClass(".PR2change")) {
      $("body").removeClass("PR2change");
      $("body").removeClass("PR3change");
      $("body").toggleClass("PR1change");

    }
    if ($(".box").hasClass("PR2box") || $(".box").hasClass("PR3box")) {

      $(".box").removeClass("PR2box");
      $(".box").removeClass("PR3box");
      $(".box").toggleClass("PR1box");

    }

  });

  $(".PR2").mouseenter(function() {
    if ($(".box").hasClass("PR2box")) {

      $(".box").removeClass("PR2box");
      $("body").removeClass("PR2change");

    }
    else {
      $(".box").toggleClass("PR2box");
      $("body").toggleClass("PR2change");
    }
    if ($("body").hasClass("PR1change") || $("body").hasClass(".PR3change")) {
      $("body").removeClass("PR1change");
      $("body").removeClass("PR3change");
      $("body").toggleClass("PR2change");

    }
    if ($(".box").hasClass("PR1box") || $(".box").hasClass("PR3box")) {

      $(".box").removeClass("PR1box");
      $(".box").removeClass("PR3box");
      $(".box").toggleClass("PR2box");

    }


  });

  $(".PR3").mouseenter(function() {
    if ($(".box").hasClass("PR3box")) {

      $(".box").removeClass("PR3box");
      $("body").removeClass("PR3change");

    }
    else {
      $(".box").toggleClass("PR3box");
      $("body").toggleClass("PR3change");
    }
    if ($("body").hasClass("PR1change") || $("body").hasClass(".PR2change")) {
      $("body").removeClass("PR2change");
      $("body").removeClass("PR1change");
      $("body").toggleClass("PR3change");

    }
    if ($(".box").hasClass("PR2box") || $(".box").hasClass("PR1box")) {

      $(".box").removeClass("PR2box");
      $(".box").removeClass("PR1box");
      $(".box").toggleClass("PR3box");

    }
    $(".box").toggleClass("PR3box");
  });
  $(".go").mouseenter(function() {
    $(".jsinfo").hide();
    $(".linuxinfo").hide();
    $(".goinfo").show();
    $(".grpcinfo").hide();
    $(".packinfo").hide();

  });

  $(".js").mouseenter(function() {
    $(".goinfo").hide();
    $(".linuxinfo").hide();
    $(".jsinfo").show();
    $(".grpcinfo").hide();
    $(".packinfo").hide();
  });

  $(".linux").mouseenter(function() {
    $(".goinfo").hide();
    $(".jsinfo").hide();
    $(".linuxinfo").show();
    $(".grpcinfo").hide();
    $(".packinfo").hide();
  });

  $(".grpc").mouseenter(function() {
    $(".goinfo").hide();
    $(".jsinfo").hide();
    $(".linuxinfo").hide();
    $(".grpcinfo").show();
    $(".packinfo").hide();

  });
  $(".pack").mouseenter(function() {
    $(".goinfo").hide();
    $(".jsinfo").hide();
    $(".linuxinfo").hide();
    $(".grpcinfo").hide();
    $(".packinfo").show();

  });

  $(".study").mouseenter(function() {
    $(".stbox").show();
    $(".stbox").css("width", "70%");
    $(".stbox").delay(1000).animate({ "opacity": "1" }, 700);
    $(".goal").addClass("grid-block");
    $(".gbox").hide();
    $(".fbox").hide();
  });

  $(".goal").mouseenter(function() {
    $(".gbox").show();
    $(".stbox").hide();
    $(".fbox").hide();
    $(".gbox").css("width", "70%");
    $(".gbox").delay(1000).animate({ "opacity": "1" }, 700);

  });
  $(".fav").mouseenter(function() {
    $(".fbox").show();
    $(".stbox").hide();
    $(".gbox").hide();
    $(".fbox").css("width", "70%");
    $(".fbox").delay(1000).animate({ "opacity": "1" }, 700);

  });






});
